<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CastController;

Route::get('/', function () {
    return view('layout.home');
});

Route::get('/table', function(){
    return view('data.table');
});

Route::get('/data-tables', function(){
    return view('data.data-tables');
});

//CRUD cast

//create
//form input data create cast
Route::get('/cast/create', 'CastController@create');
//menyimpan data ke database
Route::post('/cast', 'CastController@store');

//read
//menampilkan semua data
Route::get('/cast', 'CastController@index');
//menampilkan detail kategori berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');

//update
//form edit data cast
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//untuk update data berdasarkan id di tabel cast
Route::put('/cast/{cast_id}', 'CastController@update');

//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');

//CRUD Film
Route::resource('film', 'FilmController');