@extends('layout.master')
@section('judul')
    Halaman Tambah Cast
@endsection
@section('content')
    
<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label >Tahun</label>
      <input type="number" name="tahun" class="form-control">
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Ringkasan</label>
      <textarea name="ringkasan" class="form-control" rows="3"></textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >Poster</label>
        <input type="file" name="poster" class="form-control">
      </div>
      @error('poster')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group">
        <label >Genre</label>
        <select name="genre_id" class="form-control">
            <option value="">---Pilih Genre---</option>
            @foreach ($genre as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
      </div>
      @error('genre_id')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection