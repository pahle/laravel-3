@extends('layout.master')
@section('judul')
    Halaman Detail Film
@endsection
@section('content')

<div class="card" style="">
  <img src="{{asset('images/'.$film->poster)}}" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">{{$film->judul}}</h5>
    <p class="card-text">{{$film->ringkasan}}</p>
    <a href="/film" class="btn btn-primary">Kembali</a>
  </div>
</div>
@endsection