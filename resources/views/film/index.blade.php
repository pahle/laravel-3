@extends('layout.master')
@section('judul')
    Halaman List Film
@endsection
@section('content')
    
<a href="/film/create" class="btn btn-primary btn-sm my-2">Tambah Film</a>
<div class="row">
    <div class="col-lg-12">
        @forelse ($film as $item)
        <div class="card" style="">
          <img src="{{asset('images/'.$item->poster)}}" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">{{$item->judul}}</h5>
            <p class="card-text">{{$item->ringkasan}}</p>
            <form action="/film/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                <a href="/film/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
          </div>
        </div>
        @empty
           <h1>Data film kosong</h1>
        @endforelse
    </div>
</div>
  

@endsection